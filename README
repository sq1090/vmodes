VModeS is a library for vectorized decoding of Mode S and ADS-B messages
sent by an aircraft.

The library has the following features

1. Decode Mode S and ADS-B aircraft data

   - ICAO address
   - aircraft callsign
   - aircraft category
   - surface and airborne positions
   - altitude
   - position uncertainty

2. Validate surface and airborne positions according to ICAO standard.
3. Designed for real-time, streaming, and batching applications.
4. Use NumPy and Cython for the performance of the calculations.

Install library with::

    pip install vmodes

Read API documentation::

    pydoc vmodes

This library is inspired by pyModeS project by Junzi Sun. Learn more about
Mode S and ADS-B with his book at

    https://mode-s.org/decode/

VModeS library is licensed under terms of AGPL license, version 3, see
COPYING file for details. As stated in the license, there is no warranty,
so any usage is on your own risk.
