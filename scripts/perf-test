#!/usr/bin/env python3
#
# VModeS - vectorized decoding of Mode S and ADS-B data
#
# Copyright (C) 2020-2024 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

import numpy as np
import pyModeS as pms
import time
from binascii import unhexlify
from contextlib import contextmanager

import vmodes

N = 10 ** 6

@contextmanager
def timeit(msg):
    start = time.monotonic()
    yield
    dt = time.monotonic() - start
    n = N * len(messages)
    msg_time = 1e9 * dt / n
    print('{}: {:.2f} sec, {:.2f} ns per message'.format(msg, dt, msg_time))

messages = [
    '904ca3a33219741c85465ae1b4c3',  # df = 18
    'a8281d3030000000000000850d4a',  # df = 21
    '8d406b902015a678d4d220000000',  # example from https://mode-s.org/decode/content/ads-b/8-error-control.html
    '904ca3da121010603d04f5df3ecf',  # df = 18, tc = 2
    '977ba7ca0daa3e1915d83237c86e',  # df = 18, tc = 1
    '8d4ca2d4234994b5452820e5b7ab',  # df = 17, tc = 4
    '906a8ccc4ea51ab7b5abb26d59d5',  # altitude, barometric, qbit set
    '96300bbaa19fe7cf9efbc9478a59',  # altitude, barometric, qbit unset
]

#
# VModeS tests
#
data = vmodes.array([unhexlify(v) for v in messages])
data = data.repeat(N, axis=0)
time_data = np.arange(1000, len(data) + 1000, dtype=np.double)

print('processing {} million messages\n'.format(len(data) / 1e6))

with timeit('vmodes total'):
    with timeit('vmodes: df'):
        df_data = vmodes.df(data)

    with timeit('vmodes: typecode'):
        tc_data = vmodes.typecode(data, df_data=df_data)

    with timeit('vmodes: icao'):
        icao_data = vmodes.icao(data, df_data=df_data)

    with timeit('vmodes: category'):
        category_data = vmodes.category(data, tc_data=tc_data)

    with timeit('vmodes: callsign'):
        callsign_data = vmodes.callsign(data, tc_data=tc_data)

#    with timeit('vmodes: nuc_p'):
#        nuc_p_data = vmodes.nuc_p(data, tc_data)

    with timeit('vmodes: altitude'):
        altitude_data = vmodes.altitude(data, tc_data=tc_data)

    with timeit('vmodes: position'):
        position_data = vmodes.position(data, time_data, icao_data, tc_data)

#
# pyModeS tests
#
data = messages * N

print()

with timeit('pyModeS total'):
    with timeit('pyModeS: df'):
        df_data = [pms.df(msg) for msg in data]

    with timeit('pyModeS: typecode'):
        tc_data = [pms.typecode(msg) for msg in data]

    with timeit('pyModeS: icao'):
        icao_data = [pms.icao(msg) for msg in data]

    with timeit('pyModeS: category'):
        items = zip(tc_data, data)
        category_data = [
            pms.adsb.category(msg) for tc, msg in items if 1 <= tc <= 3
        ]

    with timeit('pyModeS: callsign'):
        items = zip(tc_data, data)
        category_data = [
            pms.adsb.callsign(msg) for tc, msg in items if 1 <= tc <= 3
        ]

    with timeit('pyModeS: nuc_p'):
        items = zip(tc_data, data)
        nuc_p_data = [
            pms.adsb.nuc_p(msg) for tc, msg in items if 5 <= tc <= 22
        ]

    with timeit('pyModeS: altitude'):
        items = zip(tc_data, data)
        altitude_data = [
            pms.adsb.altitude(msg) for tc, msg in items if 5 <= tc <= 8 or
            9 <= tc <= 18 or 20 <= tc <= 22
        ]

    with timeit('pyModeS: cpr format type'):
        altitude_data = [pms.adsb.oe_flag(msg) for msg in data]

# vim: sw=4:et:ai
